﻿using System;
using System.Collections.Generic;

namespace iqAlgorithm
{
    class Program
    {

        static int[,] iqTable = new int[10, 30]{
            {3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},

            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
        };
        static int[] IsYukuList = new int[10]{
            0,
            3,
            1,
            0,
            1,
            2,
            0,
            1,
            0,
            3
        };

        static void Main(string[] args)
        {
            int userCount = iqTable.GetLength(0);
            int currentIndex = 0;
            for (int i = 0; i < iqTable.GetLength(1); i++)
            {
                int times = userCount;
                int freeUserCount = Helper.CalcCariIstirahet(iqTable, i);
                int temp  = freeUserCount;
                
                while (freeUserCount > 0)
                {
                    if (times-- == 0)
                    {
                        System.Console.WriteLine($"Warning!!! Day: {i}. free user count: {temp}");
                        break;
                    }

                    if (currentIndex == userCount) currentIndex = 0;

                    if (iqTable[currentIndex, i] == (int)Helper.DayTypes.NULL)
                        if (Helper.UserIsReliable(iqTable, currentIndex, i, IsYukuList))
                        {
                            iqTable[currentIndex, i] = (int)Helper.DayTypes.İstirahət;
                            freeUserCount--;
                            IsYukuList[currentIndex] = 0;

                            currentIndex++;
                        }
                    else
                    {
                        currentIndex++;
                    }

                }

                Helper.FillWithWorkDay(iqTable,IsYukuList, i);
                //System.Console.WriteLine(" - - - ");
            }
           
            Helper.Show(iqTable);

            
        }

    }
}
