using System;
using System.Collections.Generic;

namespace iqAlgorithm
{
    static class Helper
    {
        public enum DayTypes : int
        {
            NULL = 0,
            İstirahət = 1,
            İş = 2,
            Məzuniyyət = 3,
            Xəstə = 4
        }
        public static void Show(int[,] array)
        {
            System.Console.Write("  ");
            for (int i = 0; i < array.GetLength(1); i++)
            {
                System.Console.Write(i + " ");
            }
            System.Console.WriteLine();
            for (int i = 0; i < array.GetLength(0); i++)
            {
                System.Console.Write(i+ " ");
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    switch(array[i, j])
                    {
                        case (int)DayTypes.İstirahət:
                            System.Console.Write($"* ");
                        break;

                        case (int)DayTypes.İş:
                            System.Console.Write($"- ");
                        break;
                        case (int)DayTypes.Xəstə:
                            System.Console.Write($"X ");
                        break;
                        case (int)DayTypes.Məzuniyyət:
                            System.Console.Write($"M ");
                        break;
                    }
                }
                System.Console.WriteLine();
            }
        }

        public static bool UserIsReliable(int[,] array, int userId, int toDay, int[] IsYukuList)
        {

            bool y = false,z = false;

            if(IsYukuList[userId]==4) return true;
            if(IsYukuList[userId]==1) return false;

           

            int day = toDay > 0 ? toDay - 1 : 0;

            if (array[userId, day] == (int)DayTypes.İş || array[userId, day] == (int)DayTypes.NULL) y = true;

            day = toDay < array.GetLength(1) - 1 ? toDay + 1 : array.GetLength(1) - 1;

            if (array[userId, day] == (int)DayTypes.İş || array[userId, day] == (int)DayTypes.NULL) z = true;
            
            //System.Console.WriteLine($" {userId} {toDay} {y && z}");


            return y && z;
        }

        public static void FillWithWorkDay(int[,] array, int[] isyukulist, int toDay)
        {
            //System.Console.WriteLine(" ----------------------");
            for (int i = 0; i < array.GetLength(0); i++)
            {
                if(isyukulist[i]==4)
                {
                    array[i, toDay] = (int)DayTypes.İstirahət;
                    isyukulist[i] = 0;
                    continue;
                }
                // System.Console.WriteLine(" Salam: " + array[i,toDay]);
                if (array[i, toDay] != (int)DayTypes.İstirahət && array[i, toDay] != (int)DayTypes.Məzuniyyət && array[i, toDay] != (int)DayTypes.Xəstə)
                {
                    array[i, toDay] = (int)DayTypes.İş;
                    isyukulist[i]+= 1;
                    //System.Console.WriteLine($" --- IŞ:{i}--");
                }

            }

            //System.Console.WriteLine(" ----------------------");
        }
        // public static bool isOnTop(UserAndIsYuku[] IsYukuList, int freeUserCount, int userId)
        // {
        //     bool result = false;
        //     for (int i = 0; i < IsYukuList.Length && i < freeUserCount; i++)
        //     {
        //         if (IsYukuList[i].UserId == userId)
        //         {
        //             result = true;
        //             break;
        //         }
        //     }

        //     return result;
        // }
        public static void ShowIsYuku(UserAndIsYuku[] list)
        {
            foreach (UserAndIsYuku item in list)
            {
                System.Console.WriteLine($"User Id: {item.UserId} - Yuk: {item.IsYuku}");
            }
        }
        public static UserAndIsYuku[] SortIsYuku(UserAndIsYuku[] list)
        {
            for (int i = 0; i < list.Length; i++)
            {
                bool sorted = true;
                for (int j = 0; j < list.Length - i - 1; j++)
                {
                    UserAndIsYuku temp;
                    if (list[j].IsYuku > list[j + 1].IsYuku)
                    {
                        temp = list[j];
                        list[j] = list[j + 1];
                        list[j + 1] = temp;
                        sorted = false;
                    }
                }
                if (sorted) break;
            }

            return list;
        }

        public static int CalcCariIstirahet(int[,] array, int today)
        {
            int count = 0;

            for (int i = 0; i < array.GetLength(0); i++)
            {
                if (array[i, today] == (int)DayTypes.NULL)
                {
                    count++;
                }
            }

            return count - QanunvericilikAlgorith(count);
        }

        public static int QanunvericilikAlgorith(int c)
        {
            return (int)Math.Round(c * 5.0 / 7);
        }

    }
}